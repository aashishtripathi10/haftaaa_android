package haftaaa.voidmain.com.haftaaa.adapter;

/**
 * Created by sanju on 18-12-2015.
 */
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import haftaaa.voidmain.com.haftaaa.ui.fragment.FragmentMobikwik;
import haftaaa.voidmain.com.haftaaa.ui.fragment.FragmentPayPal;
import haftaaa.voidmain.com.haftaaa.ui.fragment.FragmentPaytm;


public class PageAdapter extends FragmentStatePagerAdapter {

    int mNumOfTabs;

    public PageAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return new FragmentPaytm();
            case 1:
                return new FragmentPayPal();
            case 2:
                return new FragmentMobikwik();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}