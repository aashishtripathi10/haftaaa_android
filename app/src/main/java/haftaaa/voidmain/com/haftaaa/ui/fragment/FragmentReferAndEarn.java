package haftaaa.voidmain.com.haftaaa.ui.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import haftaaa.voidmain.com.haftaaa.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link FragmentReferAndEarn#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentReferAndEarn extends Fragment {

    public FragmentReferAndEarn() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment FragmentReferAndEarn.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentReferAndEarn newInstance() {
        FragmentReferAndEarn fragment = new FragmentReferAndEarn();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fragment_refer_and_earn, container, false);
        final EditText email = (EditText) view.findViewById(R.id.email);
        view.findViewById(R.id.invite).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String emailId = email.getText().toString().trim();
                if (emailId.isEmpty()) {
                    email.setError("invalid email!");
                } else {
                    //TODO: send email to specified email id with generated REFERRAL CODE.
                    Toast.makeText(getContext(), "Referral code sent to email.", Toast.LENGTH_SHORT).show();
                    Log.e("emailId", emailId);
                    String referralCode = "1234";
                    Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:"+ emailId));
                    intent.putExtra(Intent.EXTRA_TEXT, "Your referral code is "+referralCode);
                    intent.putExtra(Intent.EXTRA_SUBJECT, "Referral code invitation");
                    startActivity(Intent.createChooser(intent, "Send mail via"));
                    email.setText("");
                }
            }
        });
        return view;
    }


}
