package haftaaa.voidmain.com.haftaaa.ui.fragment;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.lang.ref.WeakReference;

import haftaaa.voidmain.com.haftaaa.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link GetStartedFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GetStartedFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private int mParam1;


    public GetStartedFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment GetStartedFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static GetStartedFragment newInstance(int param1) {
        GetStartedFragment fragment = new GetStartedFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getInt(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_demo, container, false);
        ImageView imageView = (ImageView) view.findViewById(R.id.image);
        WeakReference<ImageView> weakReference = new WeakReference<>(imageView);
        imageView = weakReference.get();
        Bitmap bitmap = null;

        if (mParam1 == 0) {
            bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.get_started_2);
        } else if (mParam1 == 1) {
            bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.get_started_3);
        } else if (mParam1 == 2) {
            bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.get_started_1);
        }
        bitmap = Bitmap.createScaledBitmap(bitmap, 340, 350, false);
        imageView.setImageBitmap(bitmap);

        return view;
    }

}
