package haftaaa.voidmain.com.haftaaa.ui.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import haftaaa.voidmain.com.haftaaa.R;
import haftaaa.voidmain.com.haftaaa.network.AppServer;
import haftaaa.voidmain.com.haftaaa.ui.fragment.GetStartedFragment;
import haftaaa.voidmain.com.haftaaa.utils.PrefManager;

public class GetStartedActivity extends AppCompatActivity implements View.OnClickListener, ViewPager.OnPageChangeListener {

    private int PAGE_COUNT = 3;
    View one, two, three;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.get_started_layout);

        initViews();
        initViewPager();
    }

    private void initViewPager() {
        ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
        assert viewPager != null;
        viewPager.addOnPageChangeListener(this);
        viewPager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return GetStartedFragment.newInstance(position);
            }

            @Override
            public int getCount() {
                return PAGE_COUNT;
            }
        });
    }

    private void initViews() {
        TextView terms = (TextView) findViewById(R.id.terms);
        TextView privacy = (TextView) findViewById(R.id.privacy);
        Button getStarted = (Button) findViewById(R.id.get_started);

        getStarted.setOnClickListener(this);
        terms.setOnClickListener(this);
        privacy.setOnClickListener(this);

        two = findViewById(R.id.two);
        one = findViewById(R.id.one);
        three = findViewById(R.id.three);
    }

    private void setWalletBackground(final PrefManager pref) {
        new AsyncTask<Void, Void, String>() {
            ProgressDialog dialog;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                dialog = new ProgressDialog(GetStartedActivity.this);
                dialog.setCancelable(false);
                dialog.setMessage("Updating Wallet...\nPlease wait.");
                dialog.show();
            }

            @Override
            protected String doInBackground(Void... params) {
                String msg = null;
                try {
                    msg = AppServer.getWallet(pref.getUserName(), pref.getUserEmail(),
                            pref.getUserWallet(), pref.getUserPhoto());
                    if (msg != null) {
                        pref.updateWallet(msg.trim());
                    }
                } catch (Exception ignored) {
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                Intent i = new Intent(GetStartedActivity.this, HomeActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
                dialog.dismiss();
            }

        }.execute();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.get_started:
                PrefManager pref = PrefManager.getInstance(this);
                if (pref == null) {
                    Toast.makeText(GetStartedActivity.this, "No SharedPrefs found!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!pref.isLoggedIn()) {
                    Toast.makeText(GetStartedActivity.this, "Login to continue.", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(GetStartedActivity.this, ReferActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                    overridePendingTransition(R.anim.right_in, R.anim.left_out);
                } else {
                    Toast.makeText(GetStartedActivity.this, "already logged in.", Toast.LENGTH_SHORT).show();
                    setWalletBackground(pref);
                }
                break;
            case R.id.terms:
                Toast.makeText(GetStartedActivity.this, "Terms of conditions!", Toast.LENGTH_SHORT).show();
                break;
            case R.id.privacy:
                Toast.makeText(GetStartedActivity.this, "Privacy policy!", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (position == 0) {
            one.setBackgroundColor(getResources().getColor(R.color.colorAccent));
            two.setBackgroundColor(Color.BLACK);
            three.setBackgroundColor(Color.BLACK);
        } else if (position == 1) {
            one.setBackgroundColor(Color.BLACK);
            two.setBackgroundColor(getResources().getColor(R.color.colorAccent));
            three.setBackgroundColor(Color.BLACK);
        } else if (position == 2) {
            one.setBackgroundColor(Color.BLACK);
            two.setBackgroundColor(Color.BLACK);
            three.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
