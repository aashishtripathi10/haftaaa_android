package haftaaa.voidmain.com.haftaaa.ui.fragment;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;

import haftaaa.voidmain.com.haftaaa.R;
import haftaaa.voidmain.com.haftaaa.utils.PrefManager;

public class FragmentProfile extends Fragment {

    private ImageView profile_image;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_profile, container, false);

        PrefManager prefs = PrefManager.getInstance(getContext());
        TextView username = (TextView) v.findViewById(R.id.username);
        TextView email = (TextView) v.findViewById(R.id.emailid);
        TextView wallet = (TextView) v.findViewById(R.id.usercredits);
        username.setText(prefs.getUserName());
        email.setText(prefs.getUserEmail());
        wallet.setText(prefs.getUserWallet());
        profile_image = (ImageView) v.findViewById(R.id.profile_image);
        loadProfileImage(prefs.getUserPhoto());

        ImageButton rateus = (ImageButton) v.findViewById(R.id.rateusimage);
        ImageButton share = (ImageButton) v.findViewById(R.id.shareusimage);
        rateus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("market://details?id=" + getActivity().getApplicationContext().getPackageName());
                Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                // To count with Play market backstack, After pressing back button,
                // to taken back to our application, we need to add following flags to intent.
                goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                        Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET |
                        Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                try {
                    startActivity(goToMarket);
                } catch (ActivityNotFoundException e) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("http://play.google.com/store/apps/details?id=" + getActivity().getApplicationContext().getPackageName())));
                }
            }


        });

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT, "Please download this cool app Haftaaa! from the PlayStore and earn amazing rewards: https://play.google.com/store/apps/details?id=com.voidmain.haftaaa");
                startActivity(intent);

            }
        });

        return v;
    }

    public void loadProfileImage(String personPhotoUrl) {
        if (TextUtils.isEmpty(personPhotoUrl)) return;
        personPhotoUrl = personPhotoUrl.substring(0, personPhotoUrl.length() - 2) + 400;
        new LoadProfileImage(profile_image).execute(personPhotoUrl);
    }

    class LoadProfileImage extends AsyncTask<String, Void, Bitmap> {

        ImageView bmImage;

        public LoadProfileImage(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);

            } catch (Exception e) {
                Log.e("Error wala", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }

    }
}

