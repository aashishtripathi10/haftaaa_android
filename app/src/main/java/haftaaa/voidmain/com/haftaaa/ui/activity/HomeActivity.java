package haftaaa.voidmain.com.haftaaa.ui.activity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import haftaaa.voidmain.com.haftaaa.R;
import haftaaa.voidmain.com.haftaaa.network.AppServer;
import haftaaa.voidmain.com.haftaaa.ui.fragment.ContentFAQ;
import haftaaa.voidmain.com.haftaaa.ui.fragment.FragmentHelp;
import haftaaa.voidmain.com.haftaaa.ui.fragment.FragmentHow;
import haftaaa.voidmain.com.haftaaa.ui.fragment.FragmentReferAndEarn;
import haftaaa.voidmain.com.haftaaa.ui.fragment.GatewayFragment;
import haftaaa.voidmain.com.haftaaa.ui.fragment.HistoryFragment;
import haftaaa.voidmain.com.haftaaa.ui.fragment.OfferWallsFragment;
import haftaaa.voidmain.com.haftaaa.utils.PrefManager;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    DrawerLayout drawer;
    LinearLayout frameContainer;

    private PrefManager pref;
    public static TextView title;
    public static TextView pointsView;
    public static ProgressBar refresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initDrawer(toolbar);

        Intent intent = new Intent();
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, 10000, PendingIntent.getBroadcast(this, 0, intent, 0));
//        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, 10000, 10000, intent);
    }

    @Override
    protected void onStart() {
        super.onStart();

        pref = PrefManager.getInstance(this);
        pointsView = (TextView) findViewById(R.id.credits);
        title = (TextView) findViewById(R.id.toolbar_text);
        refresh = (ProgressBar) findViewById(R.id.progress);
        String wallet = pref.getUserWallet();
        if (wallet.equals("0")) {
            setWalletBackground();
            refresh.setVisibility(View.VISIBLE);
        }
        pointsView.setText(wallet);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frame, OfferWallsFragment.newInstance())
                .commit();

        frameContainer = (LinearLayout) findViewById(R.id.frame);
    }

    private void setWalletBackground() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = null;

                try {
                    String userWallet = pref.getUserWallet();
                    if (!userWallet.equals("0"))
                        msg = AppServer.setWallet(pref.getUserName(), pref.getUserEmail(), userWallet, pref.getUserPhoto());
                } catch (final Exception ex) {
                    msg = "Exception:" + ex.getMessage();
                    Log.e("SetWallet#Exception", msg);
                    final String finalMsg = msg;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            refresh.setVisibility(View.GONE);
                            Toast.makeText(HomeActivity.this, finalMsg, Toast.LENGTH_LONG).show();
                        }
                    });
                }
                Log.e("ServerResWallet", msg+"");
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                Toast.makeText(HomeActivity.this, "Wallet updated.", Toast.LENGTH_LONG).show();
                HomeActivity.pointsView.setText(pref.getUserWallet());
                refresh.setVisibility(View.GONE);
            }

        }.execute();
    }

    private void initDrawer(Toolbar toolbar) {
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().findItem(R.id.nav_offerwall).setChecked(true);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        String title;
        Fragment fragment;
        int id = item.getItemId();

        if (id == R.id.nav_offerwall) {
            fragment = OfferWallsFragment.newInstance();
            title = "OfferWalls";
        } else if (id == R.id.nav_earn) {
            fragment = FragmentHow.newInstance();
            title = "How to";
        } else if (id == R.id.nav_refer) {
            fragment = FragmentReferAndEarn.newInstance();
            title = "Refer & Earn";
        } else if (id == R.id.nav_salary_gateway) {
            fragment = GatewayFragment.newInstance();
            title = "Settings";
        } else if (id == R.id.nav_history) {
            fragment = HistoryFragment.newInstance();
            title = "History";
        } else if (id == R.id.nav_faq) {
            fragment = ContentFAQ.newInstance();
            title = "FAQs";
        } else if (id == R.id.nav_support) {
            fragment = FragmentHelp.newInstance();
            title = "Support";
        } else {
            fragment = OfferWallsFragment.newInstance();
            title = "OfferWalls";
        }

        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frame, fragment)
                    .commit();
        }

        HomeActivity.title.setText(title);
//        getSupportActionBar().setTitle(title);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
