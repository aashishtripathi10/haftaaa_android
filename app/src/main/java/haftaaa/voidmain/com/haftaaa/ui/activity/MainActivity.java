package haftaaa.voidmain.com.haftaaa.ui.activity;

import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.adscendmedia.sdk.ui.OffersActivity;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.jirbo.adcolony.AdColony;
import com.jirbo.adcolony.AdColonyAd;
import com.jirbo.adcolony.AdColonyAdAvailabilityListener;
import com.jirbo.adcolony.AdColonyAdListener;
import com.jirbo.adcolony.AdColonyV4VCAd;
import com.pollfish.constants.Position;
import com.pollfish.interfaces.PollfishSurveyCompletedListener;
import com.pollfish.interfaces.PollfishSurveyNotAvailableListener;
import com.pollfish.main.PollFish;
import com.supersonic.adapters.supersonicads.SupersonicConfig;
import com.supersonic.mediationsdk.logger.SupersonicError;
import com.supersonic.mediationsdk.sdk.OfferwallListener;
import com.supersonic.mediationsdk.sdk.Supersonic;
import com.supersonic.mediationsdk.sdk.SupersonicFactory;
import com.tapjoy.TJActionRequest;
import com.tapjoy.TJConnectListener;
import com.tapjoy.TJEarnedCurrencyListener;
import com.tapjoy.TJError;
import com.tapjoy.TJGetCurrencyBalanceListener;
import com.tapjoy.TJPlacement;
import com.tapjoy.TJPlacementListener;
import com.tapjoy.TJVideoListener;
import com.tapjoy.Tapjoy;
import com.trialpay.android.TPEventStatus;
import com.trialpay.android.Trialpay;
import com.trialpay.android.TrialpayEvent;
import com.trialpay.android.TrialpayEventStatusChangeListener;
import com.vungle.publisher.AdConfig;
import com.vungle.publisher.EventListener;
import com.vungle.publisher.VunglePub;

import net.adxmi.android.AdManager;
import net.adxmi.android.interstitial.Interstitial;
import net.adxmi.android.os.EarnPointsOrderList;
import net.adxmi.android.os.OffersManager;
import net.adxmi.android.os.PointsChangeNotify;
import net.adxmi.android.os.PointsEarnNotify;
import net.adxmi.android.os.PointsManager;
import net.adxmi.android.video.VideoAdManager;
import net.adxmi.android.video.VideoAdRequestListener;
import net.adxmi.android.video.VideoRewardsListener;

import java.util.ArrayList;

import haftaaa.voidmain.com.haftaaa.R;
import haftaaa.voidmain.com.haftaaa.adapter.OfferWallAdapter;
import haftaaa.voidmain.com.haftaaa.network.AppServer;
import haftaaa.voidmain.com.haftaaa.pojo.OfferWall;
import haftaaa.voidmain.com.haftaaa.ui.fragment.FragmentHelp;
import haftaaa.voidmain.com.haftaaa.ui.fragment.FragmentProfile;
import haftaaa.voidmain.com.haftaaa.ui.fragment.FragmentRedeem;
import haftaaa.voidmain.com.haftaaa.utils.PrefManager;

/**
 * Created by sanju on 15-01-2016.
 */
public class MainActivity extends AppCompatActivity implements AdColonyAdAvailabilityListener,
        AdColonyAdListener, PollfishSurveyNotAvailableListener, PollfishSurveyCompletedListener,
        View.OnClickListener, PointsChangeNotify, PointsEarnNotify, TJPlacementListener, TJGetCurrencyBalanceListener,
        VideoRewardsListener {

    Intent adwallIntent;

    OfferwallListener mOfferwallListener;
    private PrefManager pref;
    private ListView list;

    private TrialpayEvent tpButtonOfferWall;
    private Supersonic mMediationAgent;
    private VunglePub vunglePub;
    private TJPlacement directPlayPlacement;

    private String appHash = "tbfjezqhtpl.461272122956";
    private String ZONE_ID = "vzf8889f5bd1164bc8a4";

    boolean earnedCurrency = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.mainactivity);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        toolbar_text = (TextView) toolbar.findViewById(R.id.toolbar_text);
        setSupportActionBar(toolbar);

        pref = PrefManager.getInstance(this);

        initOfferWallListener();

        ((TextView) findViewById(R.id.credits)).setText(pref.getUserWallet());

        findViewById(R.id.menu_offerwall).setOnClickListener(this);
        findViewById(R.id.menu_help).setOnClickListener(this);
        findViewById(R.id.menu_profile).setOnClickListener(this);
        findViewById(R.id.menu_wallet).setOnClickListener(this);

        String publisherId = "39945";
        String adwallId = "4267";
        adwallIntent = OffersActivity.getIntentForOfferWall(this, publisherId, adwallId, pref.getUserEmail());

        invalidateOptionsMenu();

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                initListAndSetAdapter();
            }
        });

        initGoogleAPIs();
        initAdColony();
        initTrialPay();
        initSuperSonic();
        initVungle();
//        initTapJoy();
//        initAdxmi();
        PointsManager.getInstance(this).registerPointsEarnNotify(this);
        PointsManager.getInstance(this).registerNotify(this);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startOfferWall(position);
            }
        });

    }

    private void initTapJoy() {
//        Hashtable<String, Object> connectFlags = new Hashtable<String, Object>();
//        connectFlags.put(TapjoyConnectFlag.ENABLE_LOGGING, "true");      // remember to turn this off for your production builds!

        String tapjoySDKKey = "OXQN8ThASoi_2JH7-2QekwECBCymVLpHDyz3j2ouh8MK4TcYoG5REV0MGrUq";
        Tapjoy.connect(this, tapjoySDKKey, null, new TJConnectListener() {
            @Override
            public void onConnectSuccess() {
                connected();
                Log.e("TapJoy", "Connected");
            }

            @Override
            public void onConnectFailure() {
                Log.e("TapJoy", "not connected");
            }
        });
    }

    private void initAdxmi() {
        AdManager.getInstance(this).init("6ebdab1a4d5f5595", "deb1d2c574223980");
        OffersManager.getInstance(this).onAppLaunch();
//        OffersManager.getInstance(this).showOffersWall();
        // (Optional) Register listener for OfferWall currency points, to get notification of currency points changing
        PointsManager.getInstance(this).registerNotify(this);

        // (Optional) Register listener for earning OfferWall currency points
        PointsManager.getInstance(this).registerPointsEarnNotify(this);

        // (Optional) Register listener for OfferWall currency points, to get notification of currency points earn
        VideoAdManager.getInstance(this).registerRewards(this);

        // (Optional)If jump to WebView page when click close btn.The default value is true,which means
        //enable.False means disable
        //VideoAdManager.setCloseBtnToDetail(false);

        // (Optional) If showing the Toast hint of earning  Video currency points. The default value is true, which means
        // enable. False means disable.
        //VideoAdManager.setEnableRewardsToastTips(true);

        //Loading video ads
        VideoAdManager.getInstance(this).requestVideoAd(new VideoAdRequestListener() {

            @Override
            public void onRequestSucceed() {
                Log.e("adxmi", "video request succeed");
            }

            @Override
            public void onRequestFail(int errorCode) {
                // Interpretation of the error code: -1 for the network connection fails,please check the network. -2007 for no ads, -3312
                //for the device number of plays of the day has been completed, additional error codes generally equipment problems.
                Log.e("adxmi", "video request fail="+errorCode);
            }
        });

        //(option)Interstitial ads animation,0:ANIM_NONE，1:ANIM_SIMPLE，2:ANIM_ADVANCE
        Interstitial.setAnimationType(Interstitial.ANIM_ADVANCE);
        //(option) set  Interstitial  Adxmi Label Layout to Visibility true：is yes  false: is no
        Interstitial.setLabelTag(true);

        // PreLoading Interstitial ads
        Interstitial.getInstance(this).loadAds();

        OffersManager.getInstance(this).showOffersWall();
    }

    private void initVungle() {
        vunglePub = VunglePub.getInstance();
        String vungleappid = "haftaaa.voidmain.com.haftaaa";
        vunglePub.init(this, vungleappid);
        EventListener vungleListener = new EventListener() {
            @Override
            public void onVideoView(boolean isCompletedView, int watchedMillis, int videoDurationMillis) {
                // Called each time an ad completes. isCompletedView is true if at least
                // 80% of the video was watched, which constitutes a completed view.
                // watchedMillis is for the longest video view (if the user replayed the
                // video).

                if (isCompletedView) {
                    String wallet = pref.getUserWallet();
                    if (wallet.isEmpty()) {
                        wallet = "0";
                    }
                    int points = Integer.parseInt(wallet);
                    points += 1;
                    Log.e("Vungle WalBefore", wallet);
                    pref.updateWallet(Integer.toString(points));
                    invalidateOptionsMenu();
                    Log.e("Vun wallet after", pref.getUserWallet());
                    setWalletBackground();

                }
            }

            @Override
            public void onAdStart() {
                // Called before playing an ad
            }

            @Override
            public void onAdEnd(boolean wasCallToActionClicked) {
                // Called when the user leaves the ad and control is returned to your application

            }

            @Override
            public void onAdPlayableChanged(boolean isAdPlayable) {
                // Called when the playability state changes. if isAdPlayable is true, you can now play an ad.
                // If false, you cannot yet play an ad.
            }

            @Override
            public void onAdUnavailable(String reason) {
                // Called when VunglePub.playAd() was called, but no ad was available to play

            }

        };
        vunglePub.setEventListeners(vungleListener);
    }

    private void initSuperSonic() {
        mMediationAgent = SupersonicFactory.getInstance();
        mMediationAgent.setOfferwallListener(mOfferwallListener);
        String mAppKey = "40fc6b65";
//        String userId = pref.getUserEmail();
        SupersonicConfig.getConfigObj().setClientSideCallbacks(true);
        mMediationAgent.initOfferwall(this, mAppKey, pref.getUserEmail());
    }

    private void initAdColony() {
        String APP_ID = "app8bd335b76d9a40149f";
        AdColony.configure(this, "version:1.0,store:google", APP_ID, ZONE_ID);
        AdColony.addAdAvailabilityListener(this);
    }

    private void initListAndSetAdapter() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String[] offerTitles = getResources().getStringArray(R.array.offerwallitems);
                String[] offerSubtitles = getResources().getStringArray(R.array.offerwallsubitems);
                TypedArray offerIcons = getResources().obtainTypedArray(R.array.offerwallicons);

                ArrayList<OfferWall> offerItems = new ArrayList<>();
                offerItems.add(new OfferWall(offerTitles[0], offerIcons.getResourceId(0, -1), offerSubtitles[0]));
                offerItems.add(new OfferWall(offerTitles[1], offerIcons.getResourceId(1, -1), offerSubtitles[1]));
                offerItems.add(new OfferWall(offerTitles[2], offerIcons.getResourceId(2, -1), offerSubtitles[2]));
                offerItems.add(new OfferWall(offerTitles[3], offerIcons.getResourceId(3, -1), offerSubtitles[3]));
                offerItems.add(new OfferWall(offerTitles[4], offerIcons.getResourceId(4, -1), offerSubtitles[4]));
                offerItems.add(new OfferWall(offerTitles[5], offerIcons.getResourceId(5, -1), offerSubtitles[5]));
                offerItems.add(new OfferWall(offerTitles[6], offerIcons.getResourceId(6, -1), offerSubtitles[6]));
                offerIcons.recycle();

//                int[] offerIcons = getResources().getIntArray(R.array.offerwallicons);
//
//                ArrayList<OfferWall> offerItems = new ArrayList<>();
//                for (int i = 0; i < offerItems.size(); i++) {
//                    OfferWall offerWall = new OfferWall(offerTitles[i], offerIcons[i], offerSubtitles[i]);
//                    offerItems.add(offerWall);
//                }

                list = (ListView) findViewById(R.id.list);
                OfferWallAdapter adapter = new OfferWallAdapter(MainActivity.this, offerItems);
                list.setAdapter(adapter);
            }
        });
    }

    private void initTrialPay() {
        String trialPayAppId = "f822a85ccf7863e9d4e9036c60d65ada";
        Trialpay.initApp(this, trialPayAppId);
        Trialpay.setSid(pref.getUserEmail());

        tpButtonOfferWall = Trialpay.createEvent("OfferWall", null);
        tpButtonOfferWall.setOnStatusChange(new TrialpayEventStatusChangeListener() {
            @Override
            public void statusChanged(TrialpayEvent event, TPEventStatus status) {
                Log.e("TrialPay", "OfferWall " + status.toString());
                if (TPEventStatus.HAS_OFFERS.equals(status)) {

                } else {
                }
            }
        });

        Trialpay.setRewardsListener(new Trialpay.RewardsListener() {
            // Notifies that rewards are available
            @Override
            public void onRewardAvailable() {
                // Is called when a reward is available to the user
                Trialpay.showRewards(); // display Trialpay Rewards UI
            }

            /*
             * Reward an amount of a given reward to the user.
             * The rewardId is the field set on the merchant panel, which may not match the display name of currency/product.
             * The amount is the amount of credits the user got for this rewardId.
             * It will be a lump sum of all offers that credited this reward.
             */
            @Override
            public void onReward(String currencyName, int amount) {
                //Should be accounted and added to a balance

                Toast.makeText(MainActivity.this, "Rewarded.", Toast.LENGTH_SHORT).show();
                String wallet = pref.getUserWallet();
                int wallet_point = Integer.parseInt(wallet);
                wallet_point += amount;
                Log.e("Trialpay", "user credited");
                String new_wallet = Integer.toString(wallet_point);
                pref.updateWallet(new_wallet);
                invalidateOptionsMenu();
                setWalletBackground();

            }
        });
    }

    private void startOfferWall(int position) {
        switch (position) {
            case 0:
//                if (mMediationAgent.isOfferwallAvailable()) {
                    mMediationAgent.showOfferwall();
//                } else {
//                    Toast.makeText(getApplicationContext(), "Offerwall Unavailable", Toast.LENGTH_LONG).show();
//                }
                break;
            case 1:
//                AdColonyVideoAd ad = new AdColonyVideoAd(ZONE_ID).withListener(MainActivity.this);
                AdColonyV4VCAd ad = new AdColonyV4VCAd(ZONE_ID);//.withListener(MainActivity.this);
//                Log.e("RewardAmount", ad.getRewardAmount()+"");
                ad.show();
                break;
            case 2:
//                startActivity(adwallIntent);
                initTrialPay();
                tpButtonOfferWall.fire();
                break;
            case 3:
                AdConfig config = new AdConfig();
                config.setIncentivized(true);
//                config.setIncentivizedUserId();
                config.setSoundEnabled(false);

                if (vunglePub.isAdPlayable()) {
                    vunglePub.playAd(config);
                } else {
                    Toast.makeText(getApplicationContext(), "Video unavailable , Try again later", Toast.LENGTH_LONG).show();
                }
                break;
            case 4:
                String pollFishAppId = "99f67a2c-67a5-4444-8c24-f9d77e55b187";
                PollFish.ParamsBuilder paramsBuilder = new PollFish.ParamsBuilder(pollFishAppId);
                paramsBuilder.releaseMode(true);
                paramsBuilder.indicatorPosition(Position.MIDDLE_LEFT);
                paramsBuilder.build();
                PollFish.initWith(this, paramsBuilder);
                PollFish.show();
                break;
            case 5:
//                boolean isSuccess = OffersManager.getInstance(this).checkOffersAdConfig(true);
//                if (isSuccess) {
//                    initAdxmi();
//                } else {
//                    Toast.makeText(MainActivity.this, "Offerwall not configured yet!", Toast.LENGTH_SHORT).show();
//                }
                initAdxmi();
                break;
            case 6:
                initTapJoy();
                break;
        }
    }

    public void connected() {
        directPlayPlacement = new TJPlacement(this, "Offerwall", this);
        if(Tapjoy.isConnected())
            directPlayPlacement.requestContent();
        else
            Log.d("Connected", "Tapjoy SDK must finish connecting before requesting content.");
        if(directPlayPlacement.isContentReady()) {
            directPlayPlacement.showContent();
        } else {
            //handle situation where there is no content to show, or it has not yet downloaded.
        }

        // NOTE:  The get/spend/award currency methods will only work if your virtual currency
        // is managed by Tapjoy.
        //
        // For NON-MANAGED virtual currency, Tapjoy.setUserID(...)
        // must be called after requestTapjoyConnect.

        // Setup listener for Tapjoy currency callbacks
        Tapjoy.setEarnedCurrencyListener(new TJEarnedCurrencyListener() {
            @Override
            public void onEarnedCurrency(String currencyName, int amount) {
                earnedCurrency = true;
                Log.e("Tapjoy", "You've just earned " + amount + " " + currencyName);
            }
        });

        // Setup listener for Tapjoy video callbacks
        Tapjoy.setVideoListener(new TJVideoListener() {
            @Override
            public void onVideoStart() {
                Log.e("onVideoStart", "video has started");
            }

            @Override
            public void onVideoError(int statusCode) {
                Log.e("onVideoError", "there was an error with the video: " + statusCode);
            }

            @Override
            public void onVideoComplete() {
                Log.e("onVideoComplete", "video has completed");

                // Best Practice: We recommend calling getCurrencyBalance as often as possible so the user�s balance is always up-to-date.
                Tapjoy.getCurrencyBalance(MainActivity.this);
            }
        });
    }

    private void initGoogleAPIs() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        GoogleApiClient.OnConnectionFailedListener listener = new GoogleApiClient.OnConnectionFailedListener() {
            @Override
            public void onConnectionFailed(ConnectionResult connectionResult) {

            }
        };
        GoogleApiClient mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, listener)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    private void initOfferWallListener() {
        mOfferwallListener = new OfferwallListener() {

            @Override
            public void onOfferwallInitSuccess() {
                //  Invoked when the OfferWall is prepared an ready to be shown to the user
            }

            @Override
            public void onOfferwallInitFail(SupersonicError supersonicError) {
                //  Invoked when the OfferWall does not load for the user.
            }

            @Override
            public void onOfferwallOpened() {
                //Invoked when the OfferWall successfully loads for the user.
            }

            @Override
            public void onOfferwallShowFail(SupersonicError supersonicError) {
                //Invoked when the method 'showOfferWall' is called and the OfferWall fails to load.
                //Handle initialization error here.
                //@param supersonicError - A SupersonicError Object which represents the reason of 'showOfferWall' failure.
            }

            @Override
            public boolean onOfferwallAdCredited(int credits, int totalCredits, boolean totalCreditsFlag) {

                // Invoked each time the user completes an Offer.
                // Award the user with the credit amount corresponding to the value of the â€˜creditsâ€™
                // parameter.
                // @param credits - The number of credits the user has earned.
                // @param totalCredits - The total number of credits ever earned by the user.
                // @param totalCreditsFlag - In some cases, we wonâ€™t be able to provide the exact
                // amount of credits since the last event (specifically if the user clears
                // the appâ€™s data). In this case the â€˜creditsâ€™ will be equal to the â€˜totalCreditsâ€™,
                // and this flag will be â€˜trueâ€™.
                // @return boolean - true if you received the callback and rewarded the user,
                // otherwise false.

                String superwallet = pref.getUserWallet();
                int walletpoints = Integer.parseInt(superwallet);
                Log.e("Supersonic credits be4", Integer.toString(walletpoints));
                walletpoints += credits;
                Log.e("Supersonic aftercreds", Integer.toString(walletpoints));
                String new_wallet = Integer.toString(walletpoints);
                pref.updateWallet(new_wallet);

                invalidateOptionsMenu();
                mMediationAgent.getOfferwallCredits();
                Log.e("New prefswallet", pref.getUserWallet());
                setWalletBackground();
                return true;
            }


            @Override
            public void onGetOfferwallCreditsFail(SupersonicError supersonicError) {

                // Invoked when the method 'getOfferWallCredits' fails to retrieve
                // the user's credit balance info.
                // @param supersonicError -A SupersonicError Object which represents the reason of 'getOfferWallCredits'
                // failure.

            }

            @Override
            public void onOfferwallClosed() {
                // Invoked when the user is about to return to the application after closing
                //  the Offerwall.
            }
        };
    }

    private void back() {
        if (getSupportFragmentManager().getBackStackEntryCount() >= 1) {
            getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            //ab.setTitle("Haftaaa");
//            toolbar_text.setText("Haftaaa");
            list.setVisibility(View.VISIBLE);
        }
    }

    private void backStack() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            getSupportFragmentManager().popBackStack();
            //   ab.setTitle("Haftaaa");
            list.setVisibility(View.VISIBLE);
        } else {
            this.finish();
        }
    }

    private void setWalletBackground() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg;

                try {
                    String userWallet = pref.getUserWallet();
                    if (TextUtils.isEmpty(userWallet)) {
                        userWallet = "0";
                    }
                    msg = AppServer.setWallet(pref.getUserName(), pref.getUserEmail(), userWallet, pref.getUserPhoto());
                    Log.e("Server Response wallet", msg);
                } catch (Exception ex) {
                    msg = "Error :" + ex.getMessage();
                    Log.e("RegisterActivity", "Error: " + msg);
                }
                Log.e("RegisterActivity", "AsyncTask completed: " + msg);
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                //                Toast.makeText(getApplicationContext(),
                //                      "SignUp Successfull", Toast.LENGTH_LONG)
                //                    .show();

            }
        }.execute(null, null, null);
    }

    @Override
    public void onPollfishSurveyNotAvailable() {
        Toast.makeText(MainActivity.this, "Survey Unavailable , Try again later!", Toast.LENGTH_LONG).show();
        PollFish.show();
    }

    @Override
    public void onPollfishSurveyCompleted(boolean playfulSurveys, int surveyPrice) {
        Toast.makeText(MainActivity.this, "Survey completed", Toast.LENGTH_LONG).show();
        String wallet = pref.getUserWallet();
        int wallet_points = Integer.parseInt(wallet);
        wallet_points += surveyPrice;
        Log.e("Pollfish Survey", surveyPrice + "");
        wallet = Integer.toString(wallet_points);
        Log.e("Wallet", wallet);
        pref.updateWallet(wallet);
        invalidateOptionsMenu();
        setWalletBackground();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_signin, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        boolean back = false;
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            back = true;
            backStack();
        }
        return back;
    }

    @Override
    public void onAdColonyAdAvailabilityChange(final boolean available, String zone_id) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

            }
        });
    }

    @Override
    public void onAdColonyAdAttemptFinished(AdColonyAd ad) {
        //Can use the ad object to determine information about the ad attempt:
        //ad.shown();
        //ad.notShown();
        //ad.canceled();
        //ad.noFill();
        //ad.skipped();

        if (ad.shown()) {
            String wallet = pref.getUserWallet();
            if (TextUtils.isEmpty(wallet)) {
                wallet = "0";
            }
            int points = Integer.parseInt(wallet);
            points += 1;
            Log.e("AdColony#Wallet#Before", wallet);
            pref.updateWallet(Integer.toString(points));
            invalidateOptionsMenu();
            Log.e("AdColony#Wallet#After", pref.getUserWallet());
            setWalletBackground();
        }
    }

    @Override
    public void onAdColonyAdStarted(AdColonyAd ad) {
        //Called when the ad has started playing
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        String wallet = pref.getUserWallet();
        Log.e("menu wallet", wallet + "");
        menu.findItem(R.id.action_walletaccount).setTitle(wallet);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mMediationAgent != null)
            mMediationAgent.removeOfferwallListener();
        vunglePub.clearEventListeners();
        OffersManager.getInstance(this).onAppExit();
        PointsManager.getInstance(this).unRegisterPointsEarnNotify(this);
        Tapjoy.onActivityStop(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Tapjoy.onActivityStart(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        AdColony.resume(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        AdColony.pause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        PointsManager.getInstance(this).unRegisterNotify(new PointsChangeNotify() {
            @Override
            public void onPointBalanceChange(int i) {
                Log.e("onPointBalanceChange", i + "");
            }
        });
    }

    @Override
    public void onClick(View v) {
        Fragment fragment = null;
        String name = "";
        switch (v.getId()) {
            case R.id.menu_offerwall:
                back();
                return;
            case R.id.menu_profile:
                fragment = new FragmentProfile();
                name = "Profile";
                break;
            case R.id.menu_wallet:
                fragment = new FragmentRedeem();
                name = "Redeem";
                break;
            case R.id.menu_help:
                fragment = new FragmentHelp();
                name = "Help";
                break;
        }

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.frame, fragment)
                .addToBackStack(name)
                .commit();

//        toolbar_text.setText(name);
        list.setVisibility(View.INVISIBLE);

    }

    @Override
    public void onPointBalanceChange(int i) {
        Log.e("onPointBalanceChange", i + "");
    }

    @Override
    public void onPointEarn(Context context, EarnPointsOrderList earnPointsOrderList) {
        if (earnPointsOrderList.size() > 0)
            Log.e("onPointEarn", earnPointsOrderList.get(0).getPoints() + "");
    }

    @Override
    public void onGetCurrencyBalanceResponse(String s, int i) {

    }

    @Override
    public void onGetCurrencyBalanceResponseFailure(String s) {

    }

    @Override
    public void onRequestSuccess(TJPlacement tjPlacement) {
        Log.e("onRequestSuccess", "");
    }

    @Override
    public void onRequestFailure(TJPlacement tjPlacement, TJError tjError) {

    }

    @Override
    public void onContentReady(TJPlacement tjPlacement) {

    }

    @Override
    public void onContentShow(TJPlacement tjPlacement) {

    }

    @Override
    public void onContentDismiss(TJPlacement tjPlacement) {

    }

    @Override
    public void onPurchaseRequest(TJPlacement tjPlacement, TJActionRequest tjActionRequest, String s) {

    }

    @Override
    public void onRewardRequest(TJPlacement tjPlacement, TJActionRequest tjActionRequest, String s, int i) {

    }

    @Override
    public void onVideoRewards(int i) {
        //If you also use OfferWall,you can use this method to save the video reward
        PointsManager.getInstance(this).awardPoints(i);

        //Otherwise,If you only use Video Ad,you need to save the points by yourself ,we provide an example for you
//         diySaveVideoReward(reward);
    }

//    public interface PointsChangeNotify {
//        /**
//         * Currency points remaining balance changing notification,
//         * this call-back will proceed in UI thread, can interactive directly with UI
//         * @param pointsBalance
//         *     current currency points remaining balance
//         */
//        public void onPointBalanceChange(int pointsBalance);
//    }
}
