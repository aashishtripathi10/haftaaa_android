package haftaaa.voidmain.com.haftaaa.ui.fragment;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.NotificationCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.jirbo.adcolony.AdColony;
import com.jirbo.adcolony.AdColonyAd;
import com.jirbo.adcolony.AdColonyAdAvailabilityListener;
import com.jirbo.adcolony.AdColonyAdListener;
import com.jirbo.adcolony.AdColonyV4VCAd;
import com.jirbo.adcolony.AdColonyV4VCListener;
import com.jirbo.adcolony.AdColonyV4VCReward;
import com.pollfish.constants.Position;
import com.pollfish.interfaces.PollfishSurveyCompletedListener;
import com.pollfish.interfaces.PollfishSurveyNotAvailableListener;
import com.pollfish.interfaces.PollfishUserNotEligibleListener;
import com.pollfish.main.PollFish;
import com.supersonic.adapters.supersonicads.SupersonicConfig;
import com.supersonic.mediationsdk.logger.SupersonicError;
import com.supersonic.mediationsdk.sdk.OfferwallListener;
import com.supersonic.mediationsdk.sdk.Supersonic;
import com.supersonic.mediationsdk.sdk.SupersonicFactory;
import com.tapjoy.TJConnectListener;
import com.tapjoy.Tapjoy;
import com.trialpay.android.TPEventStatus;
import com.trialpay.android.Trialpay;
import com.trialpay.android.TrialpayEvent;
import com.trialpay.android.TrialpayEventStatusChangeListener;
import com.vungle.publisher.AdConfig;
import com.vungle.publisher.EventListener;
import com.vungle.publisher.VunglePub;

import net.adxmi.android.AdManager;
import net.adxmi.android.os.EarnPointsOrderList;
import net.adxmi.android.os.OffersManager;
import net.adxmi.android.os.PointsChangeNotify;
import net.adxmi.android.os.PointsEarnNotify;
import net.adxmi.android.os.PointsManager;
import net.adxmi.android.video.VideoAdManager;
import net.adxmi.android.video.VideoRewardsListener;

import java.util.ArrayList;

import haftaaa.voidmain.com.haftaaa.R;
import haftaaa.voidmain.com.haftaaa.adapter.OfferWallAdapter;
import haftaaa.voidmain.com.haftaaa.network.AppServer;
import haftaaa.voidmain.com.haftaaa.pojo.OfferWall;
import haftaaa.voidmain.com.haftaaa.ui.activity.HomeActivity;
import haftaaa.voidmain.com.haftaaa.utils.PrefManager;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link OfferWallsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OfferWallsFragment extends Fragment implements ListView.OnItemClickListener,
        AdColonyAdAvailabilityListener, AdColonyAdListener, PollfishSurveyNotAvailableListener,
        PollfishSurveyCompletedListener, PointsChangeNotify, PointsEarnNotify,
        VideoRewardsListener {

    int AD_COLONY_NOTIFY_ID = 11;
    int VUNGLE_NOTIFY_ID = 12;
    int SUPERSONIC_NOTIFY_ID = 13;
    int TRIAL_PAY_NOTIFY_ID = 14;
    int POLLFISH_NOTIFY_ID = 15;
    int ADXMI_NOTIFY_ID = 16;

    private TrialpayEvent tpButtonOfferWall;
    private Supersonic mMediationAgent;
    private VunglePub vunglePub;

    ListView offerWallsListView;
    PrefManager pref;
    private String ZONE_ID = "vzf8889f5bd1164bc8a4";
    private boolean isAdColonyRewarded = false;

    public OfferWallsFragment() {
    }

    public static OfferWallsFragment newInstance() {
        return new OfferWallsFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AdManager.getInstance(getActivity()).init("6ebdab1a4d5f5595", "deb1d2c574223980");
        OffersManager.getInstance(getActivity()).onAppLaunch();
        PointsManager.getInstance(getActivity()).registerNotify(this);
        PointsManager.getInstance(getActivity()).registerPointsEarnNotify(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_offerwalls, container, false);
        pref = PrefManager.getInstance(getContext());

        initListView(view);

        initAdColony();
        initSuperSonic();
        initVungle();
        initTrialPay();
//        initPollfish();
//        initAdxmi();

        return view;
    }

    OfferwallListener mOfferwallListener = new OfferwallListener() {

        @Override
        public void onOfferwallInitSuccess() {
            //  Invoked when the OfferWall is prepared an ready to be shown to the user
        }

        @Override
        public void onOfferwallInitFail(SupersonicError supersonicError) {
            //  Invoked when the OfferWall does not load for the user.
        }

        @Override
        public void onOfferwallOpened() {
            //Invoked when the OfferWall successfully loads for the user.
        }

        @Override
        public void onOfferwallShowFail(SupersonicError supersonicError) {
            //Invoked when the method 'showOfferWall' is called and the OfferWall fails to load.
            //Handle initialization error here.
            //@param supersonicError - A SupersonicError Object which represents the reason of 'showOfferWall' failure.
        }

        @Override
        public boolean onOfferwallAdCredited(int credits, int totalCredits, boolean totalCreditsFlag) {

            // Invoked each time the user completes an Offer.
            // Award the user with the credit amount corresponding to the value of the â€˜creditsâ€™
            // parameter.

            String superwallet = pref.getUserWallet();
            int walletpoints = Integer.parseInt(superwallet);
            Log.d("Supersonic credits be4",Integer.toString(walletpoints));

            walletpoints += credits;
            Log.d("Supersonic aftercreds",Integer.toString(walletpoints));

            String new_wallet = Integer.toString(walletpoints);
            pref.updateWallet(new_wallet);

            Log.d("New prefswallet",pref.getUserWallet());
            setWalletBackground();

            offerwallsRewardNotification(R.drawable.vungle, "Haftaaa Reward.",
                    "You are awarded a credit.");

            return true;
        }


        @Override
        public void onGetOfferwallCreditsFail(SupersonicError supersonicError) {

            // Invoked when the method 'getOfferWallCredits' fails to retrieve
            // the user's credit balance info.
            // @param supersonicError -A SupersonicError Object which represents the reason of 'getOfferWallCredits'
            // failure.

        }

        @Override
        public void onOfferwallClosed() {
            // Invoked when the user is about to return to the application after closing
            //  the Offerwall.
        }
    };

    private void initListView(View view) {
        String[] offerTitles = getResources().getStringArray(R.array.offerwallitems);
        String[] offerSubtitles = getResources().getStringArray(R.array.offerwallsubitems);
        TypedArray offerIcons = getResources().obtainTypedArray(R.array.offerwallicons);

        ArrayList<OfferWall> offerItems = new ArrayList<>();
        offerItems.add(new OfferWall(offerTitles[0], offerIcons.getResourceId(0, -1), offerSubtitles[0]));
        offerItems.add(new OfferWall(offerTitles[1], offerIcons.getResourceId(1, -1), offerSubtitles[1]));
        offerItems.add(new OfferWall(offerTitles[2], offerIcons.getResourceId(2, -1), offerSubtitles[2]));
        offerItems.add(new OfferWall(offerTitles[3], offerIcons.getResourceId(3, -1), offerSubtitles[3]));
        offerItems.add(new OfferWall(offerTitles[4], offerIcons.getResourceId(4, -1), offerSubtitles[4]));
        offerItems.add(new OfferWall(offerTitles[5], offerIcons.getResourceId(5, -1), offerSubtitles[5]));
//        offerItems.add(new OfferWall(offerTitles[6], offerIcons.getResourceId(6, -1), offerSubtitles[6]));
        offerIcons.recycle();

//        int[] offerIcons = getResources().getIntArray(R.array.offerwallicons);
//        ArrayList<OfferWall> offerItems = new ArrayList<>();
//        for (int i = 0; i < offerItems.size(); i++) {
//            OfferWall offerWall = new OfferWall(offerTitles[i], offerIcons.getResourceId(i, -1), offerSubtitles[i]);
//            offerItems.add(offerWall);
//        }
        offerWallsListView = (ListView) view.findViewById(R.id.list);
        OfferWallAdapter adapter = new OfferWallAdapter(getContext(), offerItems);
        offerWallsListView.setAdapter(adapter);
        offerWallsListView.setOnItemClickListener(this);
    }

    private void initTapJoy() {
        String tapjoySDKKey = "OXQN8ThASoi_2JH7-2QekwECBCymVLpHDyz3j2ouh8MK4TcYoG5REV0MGrUq";
        Tapjoy.connect(getActivity(), tapjoySDKKey, null, new TJConnectListener() {
            @Override
            public void onConnectSuccess() {
//                connected();
                Log.e("TapJoy", "Connected");
            }

            @Override
            public void onConnectFailure() {
                Log.e("TapJoy", "not connected");
            }
        });
    }

    private void initAdxmi() {
        boolean isSuccess = OffersManager.getInstance(getActivity()).checkOffersAdConfig(true);
        if (isSuccess) {
//            Toast.makeText(getActivity(), "Configured", Toast.LENGTH_SHORT).show();
            Log.e("checkOffersAdConfig", "Configured");
        } else {
//            Toast.makeText(getActivity(), "not Configured!", Toast.LENGTH_SHORT).show();
            Log.e("checkOffersAdConfig", "not configured");
        }
        OffersManager.getInstance(getActivity()).showOffersWall();
        VideoAdManager.getInstance(getActivity()).registerRewards(this);
//        Interstitial.getInstance(getActivity()).loadAds();
//        VideoAdManager.getInstance(getActivity()).requestVideoAd(new VideoAdRequestListener() {
//            @Override
//            public void onRequestSucceed() {
//                Log.e("adxmi", "video request succeed");
//            }
//
//            @Override
//            public void onRequestFail(int errorCode) {
//                // Interpretation of the error code: -1 for the network connection fails,please check the network. -2007 for no ads, -3312
//                //for the device number of plays of the day has been completed, additional error codes generally equipment problems.
//                Log.e("adxmi", "video request fail=" + errorCode);
//            }
//        });

    }

    private void initVungle() {
        vunglePub = VunglePub.getInstance();
        String vungleappid = "haftaaa.voidmain.com.haftaaa";
        vunglePub.init(getActivity(), vungleappid);
        EventListener vungleListener = new EventListener() {
            @Override
            public void onVideoView(boolean isCompletedView, int watchedMillis, int videoDurationMillis) {
                if (isCompletedView) {
                    String wallet = pref.getUserWallet();
                    int points = Integer.parseInt(wallet);
                    points += 1;
                    Log.e("Vungle WalBefore", wallet);
                    pref.updateWallet(Integer.toString(points));
                    Log.e("Vun wallet after", pref.getUserWallet());
                    setWalletBackground();

                    offerwallsRewardNotification(R.drawable.vungle, "Haftaaa Reward.",
                            "You are awarded a credit point.");
                }
            }

            @Override
            public void onAdStart() {
                // Called before playing an ad
            }

            @Override
            public void onAdEnd(boolean wasCallToActionClicked) {
                // Called when the user leaves the ad and control is returned to your application

            }

            @Override
            public void onAdPlayableChanged(boolean isAdPlayable) {
                // Called when the playability state changes. if isAdPlayable is true, you can now play an ad.
                // If false, you cannot yet play an ad.
            }

            @Override
            public void onAdUnavailable(String reason) {
                // Called when VunglePub.playAd() was called, but no ad was available to play
                Snackbar.make(offerWallsListView, "No ad available to play", Snackbar.LENGTH_LONG).show();
            }

        };
        vunglePub.setEventListeners(vungleListener);
    }

    private void initSuperSonic() {
        mMediationAgent = SupersonicFactory.getInstance();
        mMediationAgent.setOfferwallListener(mOfferwallListener);
        String mAppKey = "40fc6b65";
        SupersonicConfig.getConfigObj().setClientSideCallbacks(true);
        mMediationAgent.initOfferwall(getActivity(), mAppKey, pref.getUserEmail());
    }

    private void initAdColony() {
        String APP_ID = "app8bd335b76d9a40149f";
        AdColony.configure(getActivity(), "version:1.0,store:google", APP_ID, ZONE_ID);
        AdColony.addAdAvailabilityListener(this);
        AdColony.addV4VCListener(new AdColonyV4VCListener() {
            @Override
            public void onAdColonyV4VCReward(AdColonyV4VCReward adColonyV4VCReward) {
                Log.e("onAdColonyV4VCReward", adColonyV4VCReward + " Credited");
                String wallet = pref.getUserWallet();

                isAdColonyRewarded = true;

                int points = Integer.parseInt(wallet);
                points += 1;
                Log.e("AdColony#Wallet#Before", wallet);
                pref.updateWallet(Integer.toString(points));
                Log.e("AdColony#Wallet#After", pref.getUserWallet());
                setWalletBackground();

                offerwallsRewardNotification(R.drawable.adcolony, "Haftaaa Reward.",
                        "You are awarded a credit point by AdColony.");
            }
        });

    }

    private void offerwallsRewardNotification(int icon, String title, String text) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(getContext());
        builder.setSmallIcon(icon);
        builder.setContentTitle(title);
        builder.setContentText(text);

        Notification notification = builder.build();
        NotificationManager manager = (NotificationManager) getContext()
                .getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(AD_COLONY_NOTIFY_ID, notification);
    }

    private void initTrialPay() {
        String trialPayAppId = "f822a85ccf7863e9d4e9036c60d65ada";
        Trialpay.initApp(getActivity(), trialPayAppId);
        Trialpay.setSid(pref.getUserEmail());

        tpButtonOfferWall = Trialpay.createEvent("OfferWall", null);
        tpButtonOfferWall.setOnStatusChange(new TrialpayEventStatusChangeListener() {
            @Override
            public void statusChanged(TrialpayEvent event, TPEventStatus status) {
                if (TPEventStatus.HAS_OFFERS.equals(status)) {
                    Log.e("TrialPay", status.toString());
                } else {
                    Log.e("TrialPay", status.toString());
//                    Snackbar.make(offerWallsListView, "No Offers available at this moment!", Snackbar.LENGTH_LONG).show();
//                    Toast.makeText(getContext(), "No Offers available at this moment!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        Trialpay.setRewardsListener(new Trialpay.RewardsListener() {
            // Notifies that rewards are available
            @Override
            public void onRewardAvailable() {
                // Is called when a reward is available to the user
                Trialpay.showRewards(); // display Trialpay Rewards UI
            }

            /*
             * Reward an amount of a given reward to the user.
             * The rewardId is the field set on the merchant panel, which may not match the display name of currency/product.
             * The amount is the amount of credits the user got for this rewardId.
             * It will be a lump sum of all offers that credited this reward.
             */
            @Override
            public void onReward(String currencyName, int amount) {
                //TODO: show Notification
                Toast.makeText(getActivity(), "Rewarded.", Toast.LENGTH_SHORT).show();
                String wallet = pref.getUserWallet();
                int wallet_point = Integer.parseInt(wallet);
                wallet_point += amount;
                Log.e("Trialpay", "user credited");
                String new_wallet = Integer.toString(wallet_point);
                pref.updateWallet(new_wallet);
                setWalletBackground();
            }
        });
    }

//    public void connected() {
//        TJPlacement directPlayPlacement = new TJPlacement(getActivity(), "Offerwall", this);
//        if (Tapjoy.isConnected())
//            directPlayPlacement.requestContent();
//        else
//            Log.d("Connected", "Tapjoy SDK must finish connecting before requesting content.");
//        if (directPlayPlacement.isContentReady()) {
//            directPlayPlacement.showContent();
//        } else {
//            //handle situation where there is no content to show, or it has not yet downloaded.
//        }
//
//        // NOTE:  The get/spend/award currency methods will only work if your virtual currency
//        // is managed by Tapjoy.
//        //
//        // For NON-MANAGED virtual currency, Tapjoy.setUserID(...)
//        // must be called after requestTapjoyConnect.
//
//        // Setup listener for Tapjoy currency callbacks
//        Tapjoy.setEarnedCurrencyListener(new TJEarnedCurrencyListener() {
//            @Override
//            public void onEarnedCurrency(String currencyName, int amount) {
//                Log.e("Tapjoy", "You've just earned " + amount + " " + currencyName);
//            }
//        });
//
//        // Setup listener for Tapjoy video callbacks
//        Tapjoy.setVideoListener(new TJVideoListener() {
//            @Override
//            public void onVideoStart() {
//                Log.e("onVideoStart", "video has started");
//            }
//
//            @Override
//            public void onVideoError(int statusCode) {
//                Log.e("onVideoError", "there was an error with the video: " + statusCode);
//            }
//
//            @Override
//            public void onVideoComplete() {
//                Log.e("onVideoComplete", "video has completed");
//
//                // Best Practice: We recommend calling getCurrencyBalance as often as possible so the user�s balance is always up-to-date.
//                Tapjoy.getCurrencyBalance((TJGetCurrencyBalanceListener) getContext());
//            }
//        });
//    }

    private void setWalletBackground() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = null;

                try {
                    String userWallet = pref.getUserWallet();
                    if (!userWallet.equals("0"))
                        msg = AppServer.setWallet(pref.getUserName(), pref.getUserEmail(), userWallet, pref.getUserPhoto());
                    Log.e("ServerResWallet", msg+"");
                } catch (Exception ex) {
                    msg = "Exception:" + ex.getMessage();
                    Log.e("SetWallet", msg);
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                HomeActivity.pointsView.setText(pref.getUserWallet());
            }

        }.execute();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        final ProgressDialog dialog = new ProgressDialog(getContext());
        dialog.setMessage("Loading Offerwall...\nPlease wait.");
        dialog.setCancelable(false);
        dialog.show();

        switch (position) {
            case 0:
                mMediationAgent.showOfferwall();
                break;
            case 1:
//                AdColonyVideoAd ad = new AdColonyVideoAd(ZONE_ID).withListener(this);
                AdColonyV4VCAd ad = new AdColonyV4VCAd(ZONE_ID).withListener(this);
                if (!ad.isReady()) {
                    Toast.makeText(getActivity(), "AdColony currently has no videos " +
                            "available to be played!", Toast.LENGTH_LONG).show();
                } else
                    ad.show();
                break;
            case 2:
//                initTrialPay();
                tpButtonOfferWall.fire();
                break;
            case 3:
                AdConfig config = new AdConfig();
                config.setIncentivized(true);
                config.setSoundEnabled(false);

                if (vunglePub.isAdPlayable()) {
                    vunglePub.playAd(config);
                } else {
                    Toast.makeText(getActivity(), "Video unavailable , Try again later", Toast.LENGTH_LONG).show();
                }
                break;
            case 4:
                initPollfish();
                PollFish.show();
                break;
            case 5:
                initAdxmi();
                break;
//            case 6:
//                initTapJoy();
//                break;
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dialog.dismiss();
            }
        }, 1000);
    }

    private void initPollfish() {
        String pollFishAppId = "99f67a2c-67a5-4444-8c24-f9d77e55b187";
//        PollFish.init(getActivity(), pollFishAppId, Position.MIDDLE_LEFT, 5);

        PollFish.ParamsBuilder paramsBuilder = new PollFish.ParamsBuilder(pollFishAppId);
        paramsBuilder.releaseMode(true);
        paramsBuilder.indicatorPosition(Position.MIDDLE_LEFT);
        paramsBuilder.pollfishSurveyCompletedListener(this);
        paramsBuilder.pollfishSurveyNotAvailableListener(this);
        paramsBuilder.pollfishUserNotEligibleListener(new PollfishUserNotEligibleListener() {
            @Override
            public void onUserNotEligible() {
                Snackbar.make(offerWallsListView, "User is not eligible!", Snackbar.LENGTH_LONG).show();
            }
        });
        paramsBuilder.build();

        PollFish.initWith(getActivity(), paramsBuilder);

    }

    @Override
    public void onAdColonyAdAvailabilityChange(boolean b, String s) {
        Log.e("onAdColonyAdAvailChange", "status#" + b + ", " + s);
    }

    @Override
    public void onAdColonyAdAttemptFinished(AdColonyAd adColonyAd) {
        if (adColonyAd.shown()) {
            if (!isAdColonyRewarded) {
                String wallet = pref.getUserWallet();
                if (TextUtils.isEmpty(wallet)) {
                    wallet = "0";
                }
                int points = Integer.parseInt(wallet);
                points += 1;
                Log.e("AdColony#Wallet#Before", wallet);
                pref.updateWallet(Integer.toString(points));
                Log.e("AdColony#Wallet#After", pref.getUserWallet());
                setWalletBackground();

                offerwallsRewardNotification(R.drawable.adcolony, "AdColony Reward.",
                        "You are awarded a credit point by AdColony.");
            }
        }
    }

    @Override
    public void onAdColonyAdStarted(AdColonyAd adColonyAd) {

    }

    @Override
    public void onPointBalanceChange(int i) {
        Log.e("onPointBalanceChange", i + "");
    }

    @Override
    public void onPointEarn(Context context, EarnPointsOrderList earnPointsOrderList) {
        if (earnPointsOrderList.size() > 0)
            Log.e("onPointEarn", earnPointsOrderList.get(0).getPoints() + "");
    }

    @Override
    public void onPollfishSurveyNotAvailable() {
//        Toast.makeText(getContext(), "Pollfish Survey Unavailable at the moment, Try again later!", Toast.LENGTH_LONG).show();
        Snackbar.make(offerWallsListView, "Pollfish Survey Unavailable at the moment, Try again later!", Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onPollfishSurveyCompleted(boolean playfulSurveys, int surveyPrice) {
        Snackbar.make(offerWallsListView, "Pollfish Survey completed", Snackbar.LENGTH_LONG).show();
//        Toast.makeText(getContext(), "PSurvey completed", Toast.LENGTH_LONG).show();
        String wallet = pref.getUserWallet();
        int wallet_points = Integer.parseInt(wallet);
        wallet_points += surveyPrice;
        Log.e("Pollfish Survey", surveyPrice + "");
        wallet = Integer.toString(wallet_points);
        Log.e("Wallet", wallet);
        pref.updateWallet(wallet);
        setWalletBackground();

        offerwallsRewardNotification(R.drawable.pollfish, "Haftaaa reward",
                "You are awarded a credit "+surveyPrice+" point");
    }

    @Override
    public void onVideoRewards(int i) {
        Log.e("onVideoRewards", "Video reward:" + i);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mMediationAgent != null)
            mMediationAgent.removeOfferwallListener();
        vunglePub.clearEventListeners();
        OffersManager.getInstance(getContext()).onAppExit();
        PointsManager.getInstance(getContext()).unRegisterPointsEarnNotify(this);
//        Tapjoy.onActivityStop(getActivity());
    }

    @Override
    public void onStart() {
        super.onStart();
//        Tapjoy.onActivityStart(getActivity());
    }

    @Override
    public void onResume() {
        super.onResume();
        AdColony.resume(getActivity());

    }

    @Override
    public void onPause() {
        super.onPause();
        AdColony.pause();
    }

    @Override
    public void onStop() {
        super.onStop();
        PointsManager.getInstance(getContext()).unRegisterNotify(new PointsChangeNotify() {
            @Override
            public void onPointBalanceChange(int i) {
                Log.e("onPointBalanceChange", i + "");
            }
        });
    }
}
