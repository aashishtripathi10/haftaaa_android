package haftaaa.voidmain.com.haftaaa.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import java.util.HashMap;

/**
 * Created by sanju on 02-01-2016.
 */
public class PrefManager {

    private static final String PREF_NAME = "Haftaaa";
    private static final String KEY_IS_LOGGED_IN = "isLoggedIn";
    private static final String KEY_NAME = "name";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_WALLET = "wallet";
    private static final String KEY_USER_ID = "id";
    private static final String KEY_PIC_URL = "photoUrl";
    private static final String KEY_AD_WALL_BEFORE = "before";
    private static final String KEY_AD_WALL_AFTER = "after";

    Editor editor;
    Context context;
    SharedPreferences pref;
    int PRIVATE_MODE = 0;

    public static PrefManager instance;

    public static PrefManager getInstance(Context context) {
        if (instance == null) {
            instance = new PrefManager(context);
        }
        return instance;
    }

    private PrefManager(Context context) {
        this.context = context;
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void createLogin(String name, String email, String wallet, String photo_url) {
        editor.putString(KEY_NAME, name);
        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_WALLET, wallet);
        editor.putString(KEY_PIC_URL, photo_url);
        editor.putBoolean(KEY_IS_LOGGED_IN, true);
        editor.commit();
    }

    public void updateWallet(String wallet) {
        editor.putString(KEY_WALLET, wallet);
        editor.apply();
        editor.commit();
    }

    public void updateadwallbefore(int wallet) {
        editor.putInt(KEY_AD_WALL_BEFORE, wallet);
        editor.apply();
        editor.commit();
    }

    public String getUserName() {
        return pref.getString(KEY_NAME, "null");
    }

    public String getUserEmail() {
        return pref.getString(KEY_EMAIL, "null");
    }

    public String getUserWallet() {
        if (pref.getString(KEY_WALLET, "0").equals("")) {
            return "0";
        }
        return pref.getString(KEY_WALLET, "0");
    }

    public String getUserPhoto() {
        return pref.getString(KEY_PIC_URL, "null");
    }

    public boolean isLoggedIn() {
        return pref.getBoolean(KEY_IS_LOGGED_IN, false);
    }

    public int adwallwalletefore() {
        return pref.getInt(KEY_AD_WALL_BEFORE, 0);
    }

    public int adwallwalletafter() {
        return pref.getInt(KEY_AD_WALL_AFTER, 0);
    }

    public void clearSession() {
        editor.clear();
        editor.commit();
    }

    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> profile = new HashMap<>();
        profile.put("name", pref.getString(KEY_NAME, null));
        profile.put("email", pref.getString(KEY_EMAIL, null));
        profile.put("wallet", pref.getString(KEY_WALLET, null));
        return profile;
    }

}
