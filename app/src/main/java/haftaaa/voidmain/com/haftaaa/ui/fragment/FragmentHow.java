package haftaaa.voidmain.com.haftaaa.ui.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import haftaaa.voidmain.com.haftaaa.R;

/**
 * Created by sanju on 02-01-2016.
 */
public class FragmentHow extends Fragment implements ViewPager.OnPageChangeListener {

    private int PAGE_COUNT = 3;
    View one, two, three;
    View view;
    TextView title;

    public static FragmentHow newInstance() {
        return new FragmentHow();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragmenthow, container, false);
        title = (TextView) view.findViewById(R.id.title);
        three = view.findViewById(R.id.three);
        two = view.findViewById(R.id.two);
        one = view.findViewById(R.id.one);

        initViewPager();

        return view;
    }

    private void initViewPager() {
        ViewPager viewPager = (ViewPager) view.findViewById(R.id.view_pager);
        assert viewPager != null;
        viewPager.addOnPageChangeListener(this);
        viewPager.setAdapter(new FragmentPagerAdapter(getActivity().getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return GetStartedFragment.newInstance(position);
            }

            @Override
            public int getCount() {
                return PAGE_COUNT;
            }
        });
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (position == 0) {
            one.setBackgroundColor(getResources().getColor(R.color.colorAccent));
            two.setBackgroundColor(Color.BLACK);
            three.setBackgroundColor(Color.BLACK);
            title.setText("Stay tuned with Offerwalls");
        } else if (position == 1) {
            one.setBackgroundColor(Color.BLACK);
            two.setBackgroundColor(getResources().getColor(R.color.colorAccent));
            three.setBackgroundColor(Color.BLACK);
            title.setText("Earn credits by installing App, watching Ads and Videos");
        } else if (position == 2) {
            one.setBackgroundColor(Color.BLACK);
            two.setBackgroundColor(Color.BLACK);
            three.setBackgroundColor(getResources().getColor(R.color.colorAccent));
            title.setText("Earn money at month end like Salary");
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}

