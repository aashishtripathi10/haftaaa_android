package haftaaa.voidmain.com.haftaaa.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import haftaaa.voidmain.com.haftaaa.ui.fragment.ContentFAQ;
import haftaaa.voidmain.com.haftaaa.ui.fragment.FragmentHow;


/**
 * Created by sanju on 11-02-2016.
 */
public class HelpPageAdapter extends FragmentStatePagerAdapter {

    int mNumOfTabs;

    public HelpPageAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new FragmentHow();
            case 1:
                return new ContentFAQ();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}