package haftaaa.voidmain.com.haftaaa.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import java.util.ArrayList;
import java.util.HashMap;

import haftaaa.voidmain.com.haftaaa.R;
import haftaaa.voidmain.com.haftaaa.adapter.ExpandableAdapter;

/**
 * Created by sanju on 02-01-2016.
 */
public class ContentFAQ extends Fragment {

    public static ContentFAQ newInstance() {
        return new ContentFAQ();
    }

    ArrayList<String> questionsList = new ArrayList<>();
    ArrayList<String> answersList = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmentfaq, container, false);

        initListData();

        ExpandableListView expandableListView = (ExpandableListView)
                view.findViewById(R.id.expanded_list_view);

        HashMap<String, String> listHashMap = new HashMap<>();
        for (int i = 0; i < questionsList.size(); i++) {
            listHashMap.put(questionsList.get(i), answersList.get(i));
        }

        ExpandableAdapter expandableAdapter = new ExpandableAdapter(getContext(), questionsList, listHashMap);
        expandableListView.setAdapter(expandableAdapter);

        return view;
    }

    private void initListData() {
        questionsList.add(getString(R.string.question1));
        questionsList.add(getString(R.string.question2));
        questionsList.add(getString(R.string.question3));
        questionsList.add(getString(R.string.question4));
        questionsList.add(getString(R.string.question5));
        questionsList.add(getString(R.string.question6));

        answersList.add(getString(R.string.answer1));
        answersList.add(getString(R.string.answer2));
        answersList.add(getString(R.string.answer3));
        answersList.add(getString(R.string.answer4));
        answersList.add(getString(R.string.answer5));
        answersList.add(getString(R.string.answer6));
    }

}
