package haftaaa.voidmain.com.haftaaa.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import haftaaa.voidmain.com.haftaaa.R;
import haftaaa.voidmain.com.haftaaa.pojo.OfferWall;

public class OfferWallAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<OfferWall> OfferWallItems;

    public OfferWallAdapter(Context context, ArrayList<OfferWall> offerWallItems){
        this.context = context;
        this.OfferWallItems = offerWallItems;
    }

    @Override
    public int getCount() {
        return OfferWallItems.size();
    }

    @Override
    public Object getItem(int position) {
        return OfferWallItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.offer_list_item, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        WeakReference<ImageView> imageViewWeakReference = new WeakReference<>(viewHolder.imgIcon);
        viewHolder.imgIcon = imageViewWeakReference.get();

        viewHolder.imgIcon.setImageResource(OfferWallItems.get(position).getIcon());
        viewHolder.txtTitle.setText(OfferWallItems.get(position).getTitle());
        viewHolder.subTitle.setText(OfferWallItems.get(position).getSubtitle());

        return convertView;
    }

    private class ViewHolder {
        ImageView imgIcon;
        TextView txtTitle;
        TextView subTitle;

        public ViewHolder(View convertView) {
            imgIcon = (ImageView) convertView.findViewById(R.id.thumbnail1);
            txtTitle = (TextView) convertView.findViewById(R.id.title);
            subTitle = (TextView) convertView.findViewById(R.id.offertext);
        }
    }
}
