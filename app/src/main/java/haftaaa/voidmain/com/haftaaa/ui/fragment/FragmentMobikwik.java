package haftaaa.voidmain.com.haftaaa.ui.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import haftaaa.voidmain.com.haftaaa.R;
import haftaaa.voidmain.com.haftaaa.network.AppServer;
import haftaaa.voidmain.com.haftaaa.utils.PrefManager;

public class FragmentMobikwik extends Fragment {

    String name;
    String email;
    String wallet;
    String points;
    String mobikwikId;
    String accountType = "MobiKwik";
    private EditText mobikwik;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragmentmobikwik, container, false);
        PrefManager prefs = PrefManager.getInstance(getActivity());
        Button redeem = (Button) v.findViewById(R.id.mobikwikredeem);
        mobikwik = (EditText) v.findViewById(R.id.mobikwik);

        name = prefs.getUserName();
        email = prefs.getUserEmail();
        wallet = prefs.getUserWallet();
        // accountType = "Paypal";
        points = prefs.getUserWallet();
        final int haftaaaPoints = Integer.parseInt(wallet);

        redeem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRedeemClick(haftaaaPoints);
            }
        });
        return v;
    }

    private void onRedeemClick(int haftaaaPoints) {
        mobikwikId = mobikwik.getText().toString().trim();
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        if (haftaaaPoints < 500) {
            Toast.makeText(getActivity(), "Minimum Haftaaa points should be 1000", Toast.LENGTH_LONG).show();
        } else {
            if (TextUtils.isEmpty(mobikwikId)) {

                Toast.makeText(getActivity(), "Please fill details", Toast.LENGTH_LONG).show();

            } else if (!mobikwikId.matches(emailPattern)) {
                Toast.makeText(getActivity(), "Please fill valid Id", Toast.LENGTH_LONG).show();
            } else {
                redeemPoints();
            }
        }
    }

    private void redeemPoints() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg;

                try {
                    msg = AppServer.redeemPoints(name, email, wallet, points, accountType, mobikwikId);
                    Log.d("mail gaya ", msg);
                } catch (Exception ex) {
                    msg = "Error :" + ex.getMessage();
                    Log.d("RegisterActivity", "Error: " + msg);
                }
                Log.d("RegisterActivity", "AsyncTask completed: " + msg);
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                Toast.makeText(getActivity(),
                        "Mail Sent! We will reach back to you within few hours!", Toast.LENGTH_LONG)
                        .show();
            }

        }.execute(null, null, null);
    }

}
