package haftaaa.voidmain.com.haftaaa.ui.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.PermissionChecker;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;

import haftaaa.voidmain.com.haftaaa.R;
import haftaaa.voidmain.com.haftaaa.network.AppServer;
import haftaaa.voidmain.com.haftaaa.utils.Constants;
import haftaaa.voidmain.com.haftaaa.utils.PrefManager;

/**
 * Created by sanju on 20-01-2016.
 */
public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            PermissionChecker.checkSelfPermission(this, CAMERA_SERVICE);
        }
        PermissionChecker.checkSelfPermission(this, STORAGE_SERVICE);
        PermissionChecker.checkSelfPermission(this, LOCATION_SERVICE);

        PrefManager pref = PrefManager.getInstance(this);

        if (!pref.isLoggedIn()) {
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    Intent i = new Intent(SplashScreenActivity.this, GetStartedActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                }
            }, Constants.SPLASH_TIME_OUT);
        } else {
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    Intent i = new Intent(SplashScreenActivity.this, HomeActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                }
            }, Constants.SPLASH_TIME_OUT);
        }
    }

    private void setWalletBackground(final PrefManager pref) {

        new AsyncTask<Void, Void, String>() {
            ProgressDialog dialog;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                dialog = new ProgressDialog(SplashScreenActivity.this);
                dialog.setCancelable(false);
                dialog.setMessage("Initializing details...\nPlease wait.");
                dialog.show();
            }

            @Override
            protected String doInBackground(Void... params) {
                String msg = null;
                try {
                    msg = AppServer.getWallet(pref.getUserName(), pref.getUserEmail(),
                            pref.getUserWallet(), pref.getUserPhoto());
                    Log.e("SetWallet", msg);
                    if (msg.isEmpty())
                        msg = "0";
                    pref.updateWallet(msg.trim());
                } catch (Exception ex) {
                    Log.e("setWallet#Exception", ex.getMessage() + "");
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                dialog.dismiss();
                Intent i = new Intent(SplashScreenActivity.this, HomeActivity.class);
                startActivity(i);
                finish();
            }

        }.execute();
    }
}


