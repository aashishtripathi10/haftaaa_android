package haftaaa.voidmain.com.haftaaa.network;

import android.util.Log;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import haftaaa.voidmain.com.haftaaa.utils.Constants;

public class AppServer {

    public static String register(String name, String email, String wallet, String photo_url) {
        String ret_result;
        Map<String, String> paramsMap = new HashMap<>();
        paramsMap.put(Constants.ACTION, "register");
        paramsMap.put(Constants.NAME, name);
        paramsMap.put(Constants.EMAIL, email);
        paramsMap.put(Constants.DATE, String.valueOf(Calendar.getInstance().getTime()));
        paramsMap.put(Constants.WALLET, wallet);
        paramsMap.put(Constants.PHOTO_URL, photo_url);
        ret_result = request(paramsMap);
        return ret_result;
    }

    public static String getWallet(String name, String email, String wallet, String photo_url) {
        String ret_result = "";
        URL serverUrl;
        OutputStream out;
        HttpURLConnection httpCon;
        Map<String, String> paramsMap = new HashMap<>();
        paramsMap.put(Constants.ACTION, "wallet");
        paramsMap.put(Constants.NAME, name);
        paramsMap.put(Constants.EMAIL, email);
        paramsMap.put(Constants.WALLET, wallet);
        paramsMap.put(Constants.PHOTO_URL, photo_url);
        paramsMap.put(Constants.DATE, String.valueOf(Calendar.getInstance().getTime()));
        Log.e("AppServer", "GetWALLET");

        try {
            serverUrl = new URL(Constants.URL);
            StringBuilder postBody = new StringBuilder();
            Iterator<Map.Entry<String, String>> iterator = paramsMap.entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry<String, String> param = iterator.next();
                postBody.append(param.getKey()).append('=').append(param.getValue());

                if (iterator.hasNext()) {
                    postBody.append('&');
                }
            }
            String body = postBody.toString();
            byte[] bytes = body.getBytes();
            httpCon = (HttpURLConnection) serverUrl.openConnection();
            httpCon.setDoOutput(true);
            httpCon.setUseCaches(false);
            httpCon.setFixedLengthStreamingMode(bytes.length);
            httpCon.setRequestMethod(Constants.REQUEST_POST);
            httpCon.setRequestProperty(Constants.CONTENT_TYPE, Constants.MIME_TYPE);
            out = httpCon.getOutputStream();

            out.write(bytes);
            InputStream is;
            try {
                is = httpCon.getInputStream();
                int ch;
                StringBuffer sb = new StringBuffer();
                while ((ch = is.read()) != -1) {
                    sb.append((char) ch);
                }
                ret_result = sb.toString();
                Log.e("AppServer#WalletResult", ret_result);
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("AppServer#Exception", e.getMessage()+"");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("AppServer#Exception", e.getMessage()+"");
        }
        return ret_result;
    }

    public static String setWallet(String name, String email, String wallet, String photo_url) {
        String ret_result = "";
        URL serverUrl;
        OutputStream out;
        HttpURLConnection httpCon;
        Map<String, String> paramsMap = new HashMap<>();
        paramsMap.put(Constants.ACTION, "setWallet");
        paramsMap.put(Constants.NAME, name);
        paramsMap.put(Constants.EMAIL, email);
        paramsMap.put(Constants.WALLET, wallet);
        paramsMap.put(Constants.DATE, String.valueOf(Calendar.getInstance().getTime()));
        paramsMap.put(Constants.PHOTO_URL, photo_url);

        Log.e("AppServer", "SetWALLET");

        try {
            serverUrl = new URL(Constants.URL);
            StringBuilder postBody = new StringBuilder();
            Iterator<Map.Entry<String, String>> iterator = paramsMap.entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry<String, String> param = iterator.next();
                postBody.append(param.getKey()).append('=').append(param.getValue());

                if (iterator.hasNext()) {
                    postBody.append('&');
                }
            }
            String body = postBody.toString();
            byte[] bytes = body.getBytes();
            httpCon = (HttpURLConnection) serverUrl.openConnection();
            httpCon.setDoOutput(true);
            httpCon.setUseCaches(false);
            httpCon.setFixedLengthStreamingMode(bytes.length);
            httpCon.setRequestMethod(Constants.REQUEST_POST);
            httpCon.setRequestProperty(Constants.CONTENT_TYPE, Constants.MIME_TYPE);
            out = httpCon.getOutputStream();

            out.write(bytes);

            InputStream is;

            try {
                is = httpCon.getInputStream();
                int ch;
                StringBuffer sb = new StringBuffer();
                while ((ch = is.read()) != -1) {
                    sb.append((char) ch);
                }
                ret_result = sb.toString();
                Log.e("AppServer#Wallet", ret_result);
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("AppServer#Exception", e.getMessage()+"");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("AppServer#Exception", e.getMessage()+"");
        }
        return ret_result;
    }

    public static String redeemPoints(String name, String email, String wallet, String points, String account_type, String account) {
        String ret_result;
        Map<String, String> paramsMap = new HashMap<>();
        paramsMap.put(Constants.ACTION, "redeem");
        paramsMap.put(Constants.NAME, name);
        paramsMap.put(Constants.EMAIL, email);
        paramsMap.put(Constants.WALLET, wallet);
        paramsMap.put(Constants.DATE, String.valueOf(Calendar.getInstance().getTime()));
        paramsMap.put(Constants.POINTS, points);
        paramsMap.put(Constants.ACCOUNT_TYPE, account_type);
        paramsMap.put(Constants.ACCOUNT, account);
        ret_result = request(paramsMap);
        return ret_result;
    }

    public static String request(Map<String, String> paramsMap) {
        String result;
        URL serverUrl;
        OutputStream out;
        HttpURLConnection httpCon;
        try {
            serverUrl = new URL(Constants.URL);
            StringBuilder postBody = new StringBuilder();
            Iterator<Map.Entry<String, String>> iterator = paramsMap.entrySet()
                    .iterator();
            while (iterator.hasNext()) {
                Map.Entry<String, String> param = iterator.next();
                postBody.append(param.getKey()).append('=')
                        .append(param.getValue());
                if (iterator.hasNext()) {
                    postBody.append('&');
                }
            }
            String body = postBody.toString();
            byte[] bytes = body.getBytes();
            httpCon = (HttpURLConnection) serverUrl.openConnection();
            httpCon.setDoOutput(true);
            httpCon.setUseCaches(false);
            httpCon.setFixedLengthStreamingMode(bytes.length);
            httpCon.setRequestMethod(Constants.REQUEST_POST);
            httpCon.setRequestProperty(Constants.CONTENT_TYPE, Constants.MIME_TYPE);
            out = httpCon.getOutputStream();

            out.write(bytes);

            int status = httpCon.getResponseCode();

            if (status == 200) {
                result = "success";
            } else
                result = "failure";
        } catch (Exception e) {
            Log.d("Server error is ", e.toString());
            result = "Error";
        }

        return result;
    }

}
