package haftaaa.voidmain.com.haftaaa.ui.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import haftaaa.voidmain.com.haftaaa.R;

public class ReferActivity extends AppCompatActivity {

    EditText code;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refer);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        code = (EditText) findViewById(R.id.referral_code);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = new Intent(this, SignInActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        if (item.getItemId() == R.id.action_done) {
            String referralCode = code.getText().toString();
            if (!referralCode.trim().isEmpty()) {
//                Toast.makeText(ReferActivity.this, "Valid code!", Toast.LENGTH_SHORT).show();
                validateReferralCode(referralCode);
                startActivity(intent);
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
            } else {
                code.setError("Invalid code!");
            }
        }
        if (item.getItemId() == R.id.action_skip) {
            startActivity(intent);
            overridePendingTransition(R.anim.right_in, R.anim.left_out);
        }
        return true;
    }

    private void validateReferralCode(String code) {
        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                dialog.setMessage("Validating code...\nPlease wait.");
                dialog.setCancelable(false);
                dialog.show();
            }

            @Override
            protected Void doInBackground(Void... params) {
                //TODO: check if the code is valid or not by using API
//                try {
//                    URL uri = new URL("");
//                    HttpURLConnection connection = (HttpURLConnection) uri.openConnection();
//                    connection.setRequestMethod("POST");
//                    connection.setDoOutput(true);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                dialog.dismiss();
            }
        };
    }
}
