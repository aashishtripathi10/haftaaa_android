package haftaaa.voidmain.com.haftaaa.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import haftaaa.voidmain.com.haftaaa.R;
import haftaaa.voidmain.com.haftaaa.adapter.HelpPageAdapter;

/**
 * Created by sanju on 11-02-2016.
 */
public class FragmentHelp extends Fragment {

    public static FragmentHelp newInstance() {
        return new FragmentHelp();
    }

    @Nullable @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragmenthelp,container,false);
//        TabLayout tabLayout = (TabLayout) v.findViewById(R.id.tab_layout);
//        tabLayout.addTab(tabLayout.newTab().setText("USAGE"));
//        tabLayout.addTab(tabLayout.newTab().setText("FAQ"));
//        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
//
//        final ViewPager viewPager = (ViewPager) v.findViewById(R.id.pager);
//        final HelpPageAdapter adapter = new HelpPageAdapter
//                (getFragmentManager(), tabLayout.getTabCount());
//        viewPager.setAdapter(adapter);
//        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
//        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
//            @Override
//            public void onTabSelected(TabLayout.Tab tab) {
//                viewPager.setCurrentItem(tab.getPosition());
//            }
//
//            @Override
//            public void onTabUnselected(TabLayout.Tab tab) {
//
//            }
//
//            @Override
//            public void onTabReselected(TabLayout.Tab tab) {
//
//            }
//        });

        v.findViewById(R.id.call).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Calling.", Toast.LENGTH_SHORT).show();
            }
        });
        v.findViewById(R.id.email_id).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Email sent.", Toast.LENGTH_SHORT).show();
            }
        });
        return v;
    }

}
