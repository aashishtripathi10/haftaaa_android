package haftaaa.voidmain.com.haftaaa.ui.fragment;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import haftaaa.voidmain.com.haftaaa.R;

public class GatewayFragment extends Fragment {

    EditText paypalId;
    EditText paytmId;
    EditText mobikwikId;
    ProgressDialog mProgressDialog;

    public GatewayFragment() { }

    public static GatewayFragment newInstance() {
        return new GatewayFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_gateway, container, false);

        final RadioButton paypal = (RadioButton) view.findViewById(R.id.paypal);
        final RadioButton paytm = (RadioButton) view.findViewById(R.id.paytm);
        final RadioButton mobikwik = (RadioButton) view.findViewById(R.id.mobikwik);

        paypalId = (EditText) view.findViewById(R.id.paypalId);
        paytmId = (EditText) view.findViewById(R.id.paytmId);
        mobikwikId = (EditText) view.findViewById(R.id.mobikwikId);

        setHasOptionsMenu(true);

        paytm.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    setVisibility(paytmId, paypalId, mobikwikId);

                    paytm.setChecked(true);
                    paypal.setChecked(false);
                    mobikwik.setChecked(false);
                }
            }
        });
        paypal.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    setVisibility(paypalId, paytmId, mobikwikId);

                    paypal.setChecked(true);
                    paytm.setChecked(false);
                    mobikwik.setChecked(false);
                }
            }
        });
        mobikwik.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    setVisibility(mobikwikId, paytmId, paypalId);

                    mobikwik.setChecked(true);
                    paypal.setChecked(false);
                    paytm.setChecked(false);
                }
            }
        });

        return view;
    }

    private void setVisibility(EditText visible, EditText gone1, EditText gone2) {
        visible.setVisibility(View.VISIBLE);
        gone1.setVisibility(View.GONE);
        gone2.setVisibility(View.GONE);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.gateway_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_done) {
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setMessage("Updating settings...\nPlease wait.");
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.show();
        }
        return true;
    }
}
