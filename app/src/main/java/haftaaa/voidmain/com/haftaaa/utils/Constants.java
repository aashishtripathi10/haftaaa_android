package haftaaa.voidmain.com.haftaaa.utils;

/**
 * Created by shash on 06-04-2016.
 */
public class Constants {

    public static int SPLASH_TIME_OUT = 3000;

    public static String URL = "http://1-dot-testhafta.appspot.com/haftaaa?";
    public static String ACTION = "action";
    public static String NAME = "name";
    public static String EMAIL = "email";
    public static String WALLET = "wallet";
    public static String PHOTO_URL = "photo_url";
    public static String POINTS = "points";
    public static String DATE = "date";
    public static String ACCOUNT = "account";
    public static String ACCOUNT_TYPE = "account_type";

    public static String REQUEST_GET = "GET";
    public static String REQUEST_POST = "POST";

    public static String CONTENT_TYPE = "Content-Type";
    public static String MIME_TYPE = "application/x-www-form-urlencoded;charset=UTF-8";

}
