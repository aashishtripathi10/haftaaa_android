package haftaaa.voidmain.com.haftaaa.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import haftaaa.voidmain.com.haftaaa.R;

/**
 * Created by shash on 13-05-2016.
 */
public class ExpandableAdapter extends BaseExpandableListAdapter {

    Context context;
    ArrayList<String> titleList;
    HashMap<String, String> answerList;

    public ExpandableAdapter(Context context, ArrayList<String> titleList, HashMap<String,
            String> answerList) {
        this.context = context;
        this.titleList = titleList;
        this.answerList = answerList;
    }

    @Override
    public int getGroupCount() {
        return titleList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return titleList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return answerList.get(titleList.get(groupPosition));
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String title = (String) getGroup(groupPosition);

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.faq_qstn_row_item, null);
        }

        TextView tv = (TextView) convertView.findViewById(R.id.title);
        tv.setText(title);

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        String title = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.faq_ans_row_item, null);
        }

        TextView tv = (TextView) convertView.findViewById(R.id.title);
        tv.setText(title);

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}
